package com.pason.history;

/**
 * Represents an action that is aware of how to undo/re-do itself.
 */
public interface Action {

    /**
     * Executes this action for the first time.
     */
    void execute();

    /**
     * Performs any steps necessary to undo this action.
     */
    void undo();

    /**
     * Performs any steps necessary to re-do this action.
     * <p>
     * Many implementations may choose to simply call {@link #execute()} again,
     * but certain expensive actions may choose to store state within the action
     * so that the re-do operation is faster.
     */
    void redo();
}
