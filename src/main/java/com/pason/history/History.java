package com.pason.history;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Stack;

/**
 * Represents a timeline of actions that supports operations such as undo and
 * re-do.
 * <p>
 * This class is not thread-safe.
 */
public class History {
    
    public static final String CAN_UNDO_PROPERTY = "canUndo";
    public static final String CAN_REDO_PROPERTY = "canRedo";

    private Stack<Action> undoables = new Stack<>();
    private Stack<Action> redoables = new Stack<>();

    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    /**
     * Executes the specified action and stores it in the history.
     * 
     * @param action
     *            - the action to execute.
     */
    public void execute(Action action) {
        boolean oldCanUndo = canUndo();
        boolean oldCanRedo = canRedo();

        redoables.clear();
        action.execute();
        undoables.push(action);

        updateProperties(oldCanUndo, oldCanRedo);
    }

    /**
     * Undoes the most recently-executed action, if one exists.
     */
    public void undo() {
        if(!canUndo()) {
            return;
        }

        boolean oldCanUndo = canUndo();
        boolean oldCanRedo = canRedo();

        Action action = undoables.pop();
        action.undo();
        redoables.push(action);

        updateProperties(oldCanUndo, oldCanRedo);
    }

    /**
     * Re-does the most recently undone action, if one exists.
     */
    public void redo() {
        if(!canRedo()) {
            return;
        }

        boolean oldCanUndo = canUndo();
        boolean oldCanRedo = canRedo();

        Action action = redoables.pop();
        action.redo();
        undoables.push(action);

        updateProperties(oldCanUndo, oldCanRedo);
    }

    /**
     * Determines whether this history object can currently perform an undo
     * operation.
     * 
     * @return {@code true} if an undo operation is possible; {@code false}
     *         otherwise.
     */
    public boolean canUndo() {
        return !undoables.isEmpty();
    }

    /**
     * Determines whether this history object can currently perform a re-do
     * operation.
     * 
     * @return {@code true} if a re-do operation is possible; {@code false}
     *         otherwise.
     */
    public boolean canRedo() {
        return !redoables.isEmpty();
    }

    /**
     * Clears this history object of all actions.
     */
    public void clear() {
        boolean oldCanUndo = canUndo();
        boolean oldCanRedo = canRedo();

        undoables.clear();
        redoables.clear();

        updateProperties(oldCanUndo, oldCanRedo);
    }

    /**
     * Adds the property change listener to this history object.
     * 
     * @param listener
     *            - the listener to add.
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    /**
     * Adds the property change listener to this history object for the
     * specified property name.
     * 
     * @param property
     *            - the name of the property to listen on.
     * @param listener
     *            - the listener to add.
     */
    public void addPropertyChangeListener(String property,
            PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(property, listener);
    }

    /**
     * Removes the property change listener from this history object.
     * 
     * @param listener
     *            - the listener to remove.
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    /**
     * Removes the property change listener from this history object for the
     * specified property name.
     * 
     * @param property
     *            - the name of the property the listener is listening on.
     * @param listener
     *            - the listener to remove.
     */
    public void removePropertyChangeListener(String property,
            PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(property, listener);
    }

    /**
     * Gets all property change listeners associated with this history object.
     * 
     * @return an array of all registered property change listeners.
     */
    public PropertyChangeListener[] getPropertyChangeListeners() {
        return pcs.getPropertyChangeListeners();
    }

    /**
     * Gets the property change listeners associated with the specified property
     * name for this history object.
     * 
     * @param property
     *            - the name of the property.
     * @return an array of property change listeners registered for the
     *         specified property.
     */
    public PropertyChangeListener[] getPropertyChangeListeners(String property) {
        return pcs.getPropertyChangeListeners(property);
    }

    /**
     * Ensures appropriate property change events are fired when necessary for
     * {@link #CAN_UNDO_PROPERTY} and {@link #CAN_REDO_PROPERTY}. If a given
     * value has not changed, no event will be fired for that value.
     * 
     * @param oldCanUndo
     *            - the old value of can undo.
     * @param oldCanRedo
     *            - the old value of can re-do.
     */
    protected void updateProperties(boolean oldCanUndo, boolean oldCanRedo) {
        boolean newCanUndo = canUndo();
        boolean newCanRedo = canRedo();

        pcs.firePropertyChange(CAN_UNDO_PROPERTY, oldCanUndo, newCanUndo);
        pcs.firePropertyChange(CAN_REDO_PROPERTY, oldCanRedo, newCanRedo);
    }
}
