package com.pason.history;

import com.blackcreekapps.history.Action;

/**
 * Represents a sample action for testing purposes.
 */
public class CounterAction implements Action {

    /**
     * The internal counter.
     */
    private int counter = 0;

    /**
     * Increments the internal counter.
     */
    @Override
    public void execute() {
        counter++;
    }

    /**
     * Decrements the internal counter.
     */
    @Override
    public void undo() {
        counter--;
    }

    /**
     * Increments the internal counter.
     */
    @Override
    public void redo() {
        execute();
    }

    /**
     * Gets the internal counter.
     * 
     * @return the internal counter.
     */
    public int getCounter() {
        return counter;
    }
}
