package com.pason.history;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * A special property change listener for testing.
 */
class HistoryChangeListener implements PropertyChangeListener {

    /**
     * The property to listen for.
     */
    private String property;

    /**
     * Flag indicating whether the property has changed.
     */
    private boolean changed;

    /**
     * Constructs a history property change listener for testing.
     * 
     * @param property
     *            - the property to monitor.
     */
    HistoryChangeListener(String property) {
        this.property = property;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (property.equals(evt.getPropertyName())) {
            setChanged(true);
        }
    }

    /**
     * Gets whether the specified property has changed.
     * 
     * @return a boolean indicating whether the specified property has changed.
     */
    boolean hasChanged() {
        return changed;
    }

    /**
     * Sets whether the specified property has changed.
     * 
     * @param changed
     *            - a boolean indicating whether the specified property has
     *            changed.
     */
    void setChanged(boolean changed) {
        this.changed = changed;
    }
}
