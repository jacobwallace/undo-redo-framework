package com.pason.history;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests for the {@link History} class.
 */
public class HistoryTest {

    /**
     * The history object under test.
     */
    private History history;

    /**
     * The action to use for testing purposes.
     */
    private CounterAction action;

    /**
     * Sets up the history and action objects before each test.
     */
    @Before
    public void setUp() {
        history = new History();
        action = new CounterAction();
    }

    /**
     * Test method for
     * {@link History#execute(com.pason.history.Action)}.
     */
    @Test
    public void testExecute() {
        history.execute(action);

        assertEquals(1, action.getCounter());
        assertTrue(history.canUndo());
        assertFalse(history.canRedo());
    }

    /**
     * Test method for {@link History#undo()}.
     */
    @Test
    public void testUndo() {
        history.execute(action);

        history.undo();

        assertEquals(0, action.getCounter());
        assertFalse(history.canUndo());
        assertTrue(history.canRedo());
    }

    /**
     * Test method for {@link History#redo()}.
     */
    @Test
    public void testRedo() {
        history.execute(action);
        history.undo();

        history.redo();

        assertEquals(1, action.getCounter());
        assertTrue(history.canUndo());
        assertFalse(history.canRedo());
    }

    /**
     * Test method for {@link History#clear()}.
     */
    @Test
    public void testClear() {
        history.execute(new CounterAction());
        history.execute(new CounterAction());
        history.execute(new CounterAction());

        history.clear();

        assertFalse(history.canUndo());
        assertFalse(history.canRedo());
    }

    /**
     * Test method for {@link History#canUndo()}.
     */
    @Test
    public void testCanUndo() {
        assertFalse(history.canUndo());

        history.execute(new CounterAction());

        assertTrue(history.canUndo());

        history.undo();

        assertFalse(history.canUndo());
    }

    /**
     * Test method for {@link History#canRedo()}.
     */
    @Test
    public void testCanRedo() {
        assertFalse(history.canRedo());

        history.execute(new CounterAction());

        assertFalse(history.canRedo());

        history.undo();

        assertTrue(history.canRedo());

        history.redo();

        assertFalse(history.canRedo());
    }

    /**
     * Test that a re-do is not possible following an execute.
     */
    @Test
    public void testCannotRedoAfterExecute() {
        history.execute(new CounterAction());
        history.undo();
        history.execute(new CounterAction());

        assertFalse(history.canRedo());
    }

    /**
     * Tests an undo property change listener.
     */
    @Test
    public void testUndoPropertyChangeListener() {
        String PROP = History.CAN_UNDO_PROPERTY;
        HistoryChangeListener listener = new HistoryChangeListener(PROP);
        history.addPropertyChangeListener(PROP, listener);

        history.execute(new CounterAction());

        assertTrue(listener.hasChanged());
    }

    /**
     * Tests a re-do property change listener.
     */
    @Test
    public void testRedoPropertyChangeListener() {
        String PROP = History.CAN_REDO_PROPERTY;
        HistoryChangeListener listener = new HistoryChangeListener(PROP);
        history.addPropertyChangeListener(PROP, listener);

        history.execute(new CounterAction());
        history.undo();

        assertTrue(listener.hasChanged());
    }
}
